/*
This file is part of rustlings-solutions.

rustlings-solutions is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

rustlings-solutions is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with rustlings-solutions. If not, see <https://www.gnu.org/licenses/>.
*/

// move_semantics4.rs
// Refactor this code so that instead of passing `vec0` into the `fill_vec` function,
// the Vector gets created in the function itself and passed back to the main
// function.
// Execute `rustlings hint move_semantics4` or use the `hint` watch subcommand for a hint.

fn main() {
    // let vec0 = Vec::new();

    // let mut vec1 = fill_vec(vec0);
    let mut vec1 = fill_vec();

    // println!("{} has length {} content `{:?}`", "vec1", vec1.len(), vec1);
    println!("vec1 has length {} content `{vec1:?}`", vec1.len());

    vec1.push(88);

    // println!("{} has length {} content `{:?}`", "vec1", vec1.len(), vec1);
    println!("vec1 has length {} content `{vec1:?}`", vec1.len());
}

// `fill_vec()` no longer takes `vec: Vec<i32>` as argument
fn fill_vec() -> Vec<i32> {
    // let mut vec = vec;
    let mut vec = Vec::new();

    vec.push(22);
    vec.push(44);
    vec.push(66);

    vec

    // vec![22, 44, 66]
}
